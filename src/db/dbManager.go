package db

import (
	"github.com/globalsign/mgo"
	"time"
	"sync"
)

// DBConnection is a holder for db session and other info
type DBConnection struct {
	session 	 *mgo.Session
}


var once sync.Once
var instance *DBConnection

// Connect will stablish a session between mongodb and golang server
func Connect(url string, dbName string, auth ...map[string]string) *DBConnection {

	once.Do(func() {
		instance = getSession(url, dbName, auth)
	})

	return instance
}

// 
func getSession(url string, dbName string, auth []map[string]string) *DBConnection {
	
	dialInfo := mgo.DialInfo{}

	if url != "" {
		dialInfo.Addrs = []string{url}
	}else{
		dialInfo.Addrs = []string{"localhost:27017"}
	}

	if dbName != "" {
		dialInfo.Database = dbName
	}

	if len(auth) > 0 {
		
		if u,ok := auth[0]["username"]; ok {
			dialInfo.Username = u
		}
	
		if p,ok := auth[0]["password"]; ok {
			dialInfo.Username = p
		}

	}

	dialInfo.Timeout = 10*time.Second

	session, err := mgo.DialWithInfo(&dialInfo)
	if err != nil {
		// TODO: write proper Error
		panic(err)
	}

	return &DBConnection{session: session}
	
}

// GetCollection will return the collection and a function for closing the session to DB
func (dbC *DBConnection) GetCollection(collection string, dbName ...string) (*mgo.Collection, func () ) {
	newSession := dbC.session.Copy()
	closeFunc := newSession.Close
	var c *mgo.Collection

	if len(dbName) > 0 {
		c = newSession.DB(dbName[0]).C(collection)
	}else{
		c = newSession.DB("").C(collection)
	}
	
	return c, closeFunc
}

package model

import (
	"strings"
	"web/container"
	"encoding/json"
	"github.com/globalsign/mgo/bson"
)

type todo struct{
	ID 	 bson.ObjectId			`json:"_id,omitempty" bson:"_id,omitempty"`
	Name string							`json:"name,omitempty" bson:"name,omitempty"`
	Desc string							`json:"desc,omitempty" bson:"desc,omitempty"`
	UserID bson.ObjectId		`json:"userid,omitempty" bson:"userid,omitempty"`
}

// CreateTodo create a new todo
func CreateTodo(cnt *container.Container)  {

	// TODO: validate the data
	
	body:= cnt.GetBody()
	
	var t todo
	err :=  json.NewDecoder(body).Decode(&t)
	if err != nil {
		cnt.ResponseWithError(map[string]string{"error": "the data for new todo is broken"}, "login")
		return
	}


	c,SsClose :=  cnt.DB.GetCollection("todo")
	defer SsClose()

	t.UserID = bson.ObjectIdHex(cnt.GetAuth().UserID)
	mgoErr := c.Insert(t)
	if mgoErr != nil {
		cnt.ResponseWithError(map[string]string{"error": mgoErr.Error()})
		return
	}
	
	cnt.OutputSetter("todo", "is created successfully")
	cnt.Response()
}

// DeleteTodo delete a todo by id
func DeleteTodo(cnt *container.Container)  {
	
	var t todo
	err :=  json.NewDecoder(cnt.GetBody()).Decode(&t)
	if err != nil {
		cnt.ResponseWithError(map[string]string{"error": "the data is broken"})
		return
	}
	
	c,SsClose :=  cnt.DB.GetCollection("todo")
	defer SsClose()

	mgoErr := c.RemoveId(t.ID)
	if mgoErr != nil {
		cnt.ResponseWithError(map[string]string{"error": mgoErr.Error()})
		return
	}

	cnt.OutputSetter("todo", "is removed successfully")
	cnt.Response()
}

// UpdateTodo update a todo by id
func UpdateTodo(cnt *container.Container)  {
		
	var t todo
	err :=  json.NewDecoder(cnt.GetBody()).Decode(&t)
	if err != nil {
		cnt.ResponseWithError(map[string]string{"error": "the data is broken"})
		return
	}
	
	c,SsClose :=  cnt.DB.GetCollection("todo")
	defer SsClose()

	
	updateValue := bson.M{}
	if len(t.Name) > 0 {
		updateValue["name"] = t.Name
	}
	if len(t.Desc) > 0 {
		updateValue["desc"] = t.Desc
	}


	mgoErr := c.UpdateId(t.ID, bson.M{"$set": updateValue})
	if mgoErr != nil {
		cnt.ResponseWithError(map[string]string{"error": mgoErr.Error()})
		return
	}

	cnt.OutputSetter("todo", "is updated successfully")
	cnt.Response()
}

// GetAllTodos get the list of all todos
func GetAllTodos(cnt *container.Container)  {

	c,SsClose :=  cnt.DB.GetCollection("todo")
	defer SsClose()

	userID := cnt.GetAuth().UserID
	if len(userID) < 1 {
		cnt.ResponseWithError(map[string]string{"token error": "token is not valid or set"}, "login")
	}

	var t []todo
	mgoErr := c.Find(bson.M{"userid": bson.ObjectIdHex(userID)}).All(&t)
	if mgoErr != nil {
		cnt.ResponseWithError(map[string]string{"error": mgoErr.Error()})
		return
	}

	cnt.OutputSetter("todoList", t)
	cnt.Response()
}

// SearchTodo search a todo by name
func SearchTodo(cnt *container.Container)  {
	
	// TODO: validate the data
	
	body:= cnt.GetBody()
	
	var t todo
	err :=  json.NewDecoder(body).Decode(&t)
	if err != nil {
		cnt.ResponseWithError(map[string]string{"error": "the data for new todo is broken"}, "login")
		return
	}


	c,SsClose :=  cnt.DB.GetCollection("todo")
	defer SsClose()

	searchQuery := t.getSearchQuery()
	userIDQuery := bson.M{"userid": bson.ObjectIdHex(cnt.GetAuth().UserID)}
	query := bson.M{"$and": []bson.M{userIDQuery, searchQuery}}
	
	var result []todo
	mgoErr := c.Find(query).All(&result)
	if mgoErr != nil {
		cnt.ResponseWithError(map[string]string{"error": mgoErr.Error()})
		return
	}
	
	cnt.OutputSetter("todoList", result)
	cnt.Response()
}

// GetTodo get a todo by id
func GetTodo(cnt *container.Container)  {
	// TODO: validate the data

	c,SsClose :=  cnt.DB.GetCollection("todo")
	defer SsClose()

	userID := cnt.GetAuth().UserID
	todoID := cnt.GetParams()

	userQuery := bson.M{"userid": bson.ObjectIdHex(userID)}
	todoQuery := bson.M{"_id": bson.ObjectIdHex(todoID["id"].(string))}

	var result todo
	mgoErr := c.Find(bson.M{"$and": []bson.M{userQuery, todoQuery}}).One(&result)
	if mgoErr != nil {
		cnt.ResponseWithError(map[string]string{"error": mgoErr.Error()})
		return
	}
	
	cnt.OutputSetter("todo", result)
	cnt.Response()
}

// GetTodoByPublicLink get a todo by id
func GetTodoByPublicLink(cnt *container.Container)  {
	
}


func (t todo) getSearchQuery() bson.M {
	
	namesQuery := bson.M{}
	descQuery := bson.M{}
	if len(t.Name) > 0 {
		namesQuery["name"] = bson.RegEx{Pattern: t.Name, Options:""}
	}
	
	if len(t.Desc) > 0 {
		descs := strings.Split(strings.Trim(t.Desc, "&"), "&")
		if len(descs) > 0 {
			pattern := strings.ReplaceAll(t.Desc, "&", ".*")
			descQuery["desc"] = bson.RegEx{Pattern: pattern, Options:""}

		}	else {
			descQuery["desc"] = bson.RegEx{Pattern: t.Desc, Options:""}

		}
		
	}

	query := bson.M{}
	if len(descQuery) > 0 && len(namesQuery) > 0 {
		query = bson.M{"$or": []bson.M{namesQuery, descQuery}}

	} else if len(namesQuery) > 0 {
		query = namesQuery
	
	} else if len(descQuery) > 0 {
		query = descQuery
	
	}

	
	return query
}
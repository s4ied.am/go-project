package model

import (
	"web/container"
)

// Home return home page information
func Home(cnt *container.Container)  {
	info := make(map[string]interface{})
	info["about"] = "It's an online service for managing todos written in GO"
	info["testimonials"] = map[string]string{"john": "it's great", "mary": "it's easy to use", "iglesias": "it's fast and secure"}
	cnt.OutputSetter("info", info)
	cnt.Response()
}

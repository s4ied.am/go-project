package user

import (
	"db"
	h "helper"
	"github.com/globalsign/mgo/bson"
)


// User is a type for information of the user
type User struct{
	ID 	 		 bson.ObjectId		`json:"_id,omitempty" bson:"_id,omitempty"`
	UserName h.Username				`json:"username,omitempty" bson:"username,omitempty"`
	Password h.Password				`json:"pass,omitempty" bson:"pass,omitempty"`
	Email 	 h.Email					`json:"email,omitempty" bson:"email,omitempty"`
}



// GetAllUsers get a list of user by the value
func GetAllUsers(filed string, value interface{}) []User {
	
	db := db.Connect("", "test")
	c, cs := db.GetCollection("user","")
	defer cs()

	var users []User
	err := c.Find(bson.M{filed: value}).All(&users)
	if err != nil {
		// TODO: set err
		return nil
	}

	return users
}

// GetUserByID get User info by id 
func GetUserByID(id int) (*User) {
	users := GetAllUsers("_id", id)
	if len(users) != 1 {
		return nil
	}
	
	return &users[0]
}

// GetUserByUsername get user by username
func GetUserByUsername(username string) *User {
	users := GetAllUsers("username", username)
	if len(users) != 1 {
		return nil
	}
	
	return &users[0]
}

// GetUserByEmail get user by email
func GetUserByEmail(email string) *User {
	users := GetAllUsers("email", email)
	if len(users) != 1 {
		return nil
	}
	
	return &users[0]
}
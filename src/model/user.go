package model

import (
	"time"
	h "helper"
	"helper/auth"
	"web/container"
	"encoding/json"
	"github.com/globalsign/mgo/bson"
	"model/user"
)

// Login function for login by username and password
func Login(cnt *container.Container)  {

	body := cnt.GetBody()
	var u user.User
	err := json.NewDecoder(body).Decode(&u)
	if err != nil {
		cnt.ResponseWithError(map[string]string{"error": err.Error()}, "login")
		return
	}

	c, SsClose := cnt.DB.GetCollection("user")
	defer SsClose()

	passQuery := bson.M{"pass": u.Password}

	findQuery := bson.M{}
	if len(u.UserName) > 0 {
		findQuery = bson.M{"username": u.UserName}
	} else if len(u.Email) > 0 {
		findQuery = bson.M{"email": u.Email}
	} else {
		cnt.ResponseWithError(map[string]string{"error": "no username or email"}, "login")
		return
	}


	err = c.Find(bson.M{"$and": []bson.M{findQuery, passQuery}}).One(&u)
	if err != nil {
		cnt.ResponseWithError(map[string]string{"error": "there is no such user"}, "login")
		return
	}


	tokenS := auth.TokenStruct{}
	tokenS.UserID = u.ID.Hex()
	tokenS.UserName = string (u.UserName)
	
	// expire time set for 2 hours
	token, err := auth.CreatToken(time.Now().Add(2*time.Hour), tokenS)

	if err != nil {
		cnt.ResponseWithError(map[string]string{"error": err.Error()}, "login")
		return
	}
	cnt.OutputSetter("token", token.Token)
	cnt.Response()
}

// SignUp log the user in 
func SignUp(cnt *container.Container)  {
	// params := cnt.GetParams()
	w := cnt.GetWriter()
	
	body := cnt.GetBody()
	var u user.User
	err := json.NewDecoder(body).Decode(&u)
	if err != nil {
		// fmt.Fprintln(w, err)
		return
	}

	vErr := h.VErrs{}
	
	u.Password.Validate(&vErr, "min:8")
	u.UserName.Validate(&vErr, "unique:user>username")
	u.Email.Validate(&vErr, "unique:user>email,username", "email")

	
	if fieldErr, err :=  vErr.IsFailed(); err != nil {
		w.Header().Set("content-type", "application/json")
		json.NewEncoder(w).Encode(fieldErr)
		// fmt.Fprintln(w, fieldErr)
		return 
	}

	c,SsClose := cnt.DB.GetCollection("user")
	defer SsClose()

	u.ID = bson.NewObjectId()
	err = c.Insert(&u)
	if err != nil {
		// fmt.Fprintln(w, err)
		return
	}
	w.Header().Set("content-type", "application/json")
	json.NewEncoder(w).Encode(u)
	// err := validator.Pass("")
}


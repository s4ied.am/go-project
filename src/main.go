package main

import (
	"fmt"
	"myserver"
)

// Run more than one server with Go routins
func main() {
	myserver.RunServer()
	fmt.Println("Done")
} 


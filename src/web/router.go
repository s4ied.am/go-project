package web

import (
	"net/http"
	"strings"
	"time"
	"web/container"
	"helper/auth"
)

// Route structure of each route
type Route struct {
	Method      string
	Pattern     string
	HandlerFunc func (*container.Container)
}

// Routes is a container of defined routes
type Routes []Route

// urlInfo store the info of request URL
type urlInfo struct{
	Path []string
	Len int
}

// Router return a function associate with the route
func Router(w http.ResponseWriter, req *http.Request)  {
	cnt := container.GetInstance(w, req)
	var m = false
	var params = make(map[string] interface{})

	path, l := pathSplitter(req.URL.Path)
	url := urlInfo{path,l}
	
	for k, v := range req.Form {
		if len(v) > 1  {
			params[k] = v
		} else {
			params[k] = v[0]
		}
	}
	cnt.SetParams(params)

	// public pages
	for _, r := range allRoutes {
		m, params = url.matchPattern(r.Pattern)
		
		if m && r.Method == req.Method {
			
			cnt.AppendParams(params)

			r.HandlerFunc(cnt)
			return
		}
	}

	// authorized users
	if Authorized(cnt) {
		for _, r := range primeUser {
			m, params = url.matchPattern(r.Pattern)
			
			if m && r.Method == req.Method {
				
				cnt.AppendParams(params)
	
				r.HandlerFunc(cnt)
				return
			}
		}
	}

}


// matchUrlPattern check the url against the routes
func (url urlInfo) matchPattern(routePath string) (bool, map[string]interface{}) {

	routeSplit, lenR	:= pathSplitter(routePath)

	params := make(map[string] interface{})

	if lenR != url.Len {
		return false, nil
	}

	for i := 0; i < len(routeSplit); i++ {
		holder := strings.HasPrefix(routeSplit[i], "{")
		
		if holder {
			params[strings.Trim(routeSplit[i], "{}")] = url.Path[i]
		
		}else if routeSplit[i] != url.Path[i] { 
			return false, nil 
		}
	}

	return true, params	
}

// pathSplitter split the path then return the array and the length of it
func pathSplitter(s string) ([]string, int) {
	sp := strings.Split( strings.Trim(s, "/"), 	"/")

	return sp, len(sp) 
}

// Authorized check the token for authorization
func Authorized(cnt *container.Container) bool {
	req := cnt.GetRequest()
	body := cnt.GetBody()
	jwt, err := auth.GetTokenFromRequest(req, body).Validate()
	
	if err != nil {
		cnt.ResponseWithError(map[string]string{"token error": err.Error()}, "login")
		return false
	}
	
	cnt.SetAuth(&jwt)

	// after one hour reset token
	if time.Now().Unix() - jwt.IssuedAt > 3600  {
		cnt.OutputSetter("tokenReset", true)
	}

	return true
}

// GET define a route for GET Request
func GET(pattern string, f func (*container.Container), middleWare ...func()) Route {
	return Route{"GET", pattern, f}	
}

// POST define a route for POST Request
func POST(pattern string, f func (*container.Container)) Route {
	return Route{"POST", pattern, f}	
}

// PUT define a route for PUT Request
func PUT(pattern string, f func (*container.Container)) Route {
	return Route{"PUT", pattern, f}	
}

// DELETE define a route for DELETE Request
func DELETE(pattern string, f func (*container.Container)) Route {
	return Route{"DELETE", pattern, f}	
}
package web

import (
	"model"
)

var allRoutes = Routes{
	GET("/", model.Home),	
	POST("/login", model.Login),
}

var primeUser = Routes{
	GET("/todo", model.GetAllTodos),
	GET("/todo/{id}", model.GetTodo),
	POST("/todo", model.CreateTodo),
	PUT("/todo", model.UpdateTodo),
	DELETE("/todo", model.DeleteTodo),
	
	POST("/todo/search", model.SearchTodo),
	
}

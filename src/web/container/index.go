package container


import (
	"sync"
	"net/http"
	"bytes"
	"io/ioutil"
	"db"
	"helper/auth"
	"helper/response"
)

// Container has all the data need to be send to components
type Container struct{
	l sync.Mutex 
	wg sync.WaitGroup
	
	data map[string] interface{}
	outPut map[string] interface{}
	params map[string] interface{}
	request *http.Request
	writer http.ResponseWriter
	auth *auth.TokenStruct
	body []byte
	DB *db.DBConnection
	Ch chan bool
}

// GetInstance create one instance of the package
func GetInstance(w http.ResponseWriter, req *http.Request) *Container {

	b, _ := ioutil.ReadAll(req.Body)

	return &Container{
		data: make(map[string] interface{}), 
		outPut: make(map[string] interface{}), 
		params: make(map[string] interface{}), 
		request: req,
		writer: w,
		auth: nil,
		body: b,
		DB: db.Connect("", "test"),
		Ch: make(chan bool, 1),
	}
}

// Setter set variable safely 
func (c *Container) Setter(field string, val interface{}) bool {
	c.l.Lock()
	defer c.l.Unlock()

	c.data[field] = val
	
	return true
}

// Getter Get the variable safely 
func (c *Container) Getter(field string) (interface{}, bool) {
	
	c.l.Lock()
	defer c.l.Unlock()
	val, err := c.data[field]

	return val, err
}

// OutputSetter add data for respond to user
func (c *Container) OutputSetter(field string, val interface{}) bool {
	c.l.Lock()
	defer c.l.Unlock()

	c.outPut[field] = val
	
	return true
}

// SetParams set parameters in the object safely
func (c *Container) SetParams(p map[string] interface{}) bool {
	c.l.Lock()
	defer c.l.Unlock()

	c.params = p
	return true
}

// AppendParams appends a map[string]interface to params
func (c *Container) AppendParams(p map[string] interface{}) bool {
	c.l.Lock()
	defer c.l.Unlock()

	for k, v := range p {
		c.params[k] = v
	}
	return true
}

// GetParams return the parameter value safely
func (c *Container) GetParams() map[string] interface{} {
	c.l.Lock()
	defer c.l.Unlock()

	return c.params
}


// GetRequest return the request value safely
func (c *Container) GetRequest() *http.Request {
	c.l.Lock()
	defer c.l.Unlock()

	return c.request
}


// GetWriter return the writer object value safely
func (c *Container) GetWriter() http.ResponseWriter {
	c.l.Lock()
	defer c.l.Unlock()

	return c.writer
}

// WgAdd increment the waitGroup by i for goroutine safety
func (c *Container) WgAdd(i int) {
	c.l.Lock()
	defer c.l.Unlock()

	c.wg.Add(i)
}
 
// WgDone decrement by 1 after a goroutine is finished
func (c *Container) WgDone() {
	c.l.Lock()
	defer c.l.Unlock()

	c.wg.Done()
}

// IsDone check whether all goroutine all done or not
func (c *Container) IsDone() {
	c.l.Lock()
	defer c.l.Unlock()

	c.wg.Wait()
}


// GetBody return the value of body in the container object
func (c *Container) GetBody() *bytes.Buffer {
	c.l.Lock()
	defer c.l.Unlock()
	
	return bytes.NewBuffer(c.body)
}

// Response create a response to user
func (c *Container) Response() {

	response.JSONResponseWriter(c.writer, c.outPut, 200)
}

// ResponseWithError create a response with error
func (c *Container) ResponseWithError(err map[string]string, redirectTo ...string) {
	if len(redirectTo) > 0 {
		// TODO: get base URL from env
		c.OutputSetter("redirect", redirectTo[0])
	}
	c.l.Lock()
	for k, v := range err {
		c.outPut[k] = v
	}
	c.l.Unlock()

	response.JSONResponseWriter(c.writer, c.outPut, 300)
}


// GetAuth return the value of body in the container object
func (c *Container) GetAuth() *auth.TokenStruct {
	c.l.Lock()
	defer c.l.Unlock()
	
	return c.auth
}

// SetAuth will set new value for auth property
func (c *Container) SetAuth(auth *auth.TokenStruct) bool {
	c.l.Lock()
	defer c.l.Unlock()
	
	c.auth = auth
	return true
}
package helper

import (
	"time"
	"fmt"
	"regexp"
	"errors"
	"sync"
	// "log"
	"strings"
	"strconv"
	"db"
	// "reflect"
)


func validationOperator(s interface{}, rules []string) error {
	err := make(chan error)
	var wg sync.WaitGroup
	l := len(rules)

	wg.Add(l)
	for _, rule := range rules {
		
		go func (val interface{}, fnc string)  {
			defer wg.Done()

			f := strings.Split(fnc, ":")
			f[0] = strings.Trim(f[0], " ")

			switch f[0] {
			case "unique":
				unique(err, val, f)
				break
			case "min":
				min(err, val, f)
				break
			case "email":
				email(err, val)
				break
			case "regex":
				customValidation(err, val, f)
				break
			default:
			}
			// end switch

		}(s, rule) // end gopher
	
	} // end loop

	// it will wait until all validations are finished or an error is occurred
	go func ()  {
		wg.Wait()
		err<-nil
	}()

	if msg := <-err; msg != nil {
		return msg
	}	

	return nil
}


// IsFailed check Errors considering the concurrency pattern
func (errs *VErrs) IsFailed() (VErrs, error) {
	t := time.After(10*time.Second)
	myErr := VErrs{}
	var opErr error

	for _, err := range *errs {
		select {
		case e := <-err.c:
			
			if e {
				myErr = append(myErr, err)
				opErr = errors.New("notValid")
			}

		case <-t:
			
			return myErr, errors.New("timeOut")
		}
	}

	return myErr, opErr
}


func unique(cErr chan<- error, s interface{}, param []string){
	if len(param) < 2 {
		cErr <- errors.New("the filed is not set")
	}

	tableField:= strings.Split(param[1], ">")
	tb := strings.Trim(tableField[0], " ")
		
	db := db.Connect("", "test")
	c, cs := db.GetCollection(tb,"")
	defer cs()
	
	type bson map[string]interface{}
	orQuery := [] bson{} 
	if len(tableField) > 1 {
		fields := strings.Split(tableField[1], ",")
		
		for _, f := range fields {
			orQuery = append(orQuery, bson{f:s})
		}
	}
	

	count, err := c.Find(bson{"$or": orQuery}).Count()
	if err != nil {
		cErr <- errors.New("database connection problem")
	}

	if count > 0 {
		cErr <- errors.New("it isn't unique")
	}
}

func min(cErr chan<- error, s interface{}, param []string) {

	
	minLength, _ := strconv.Atoi(strings.Trim(param[1], " "))

	a := fmt.Sprintf("%v", s)
	if len(a) < minLength {

		cErr <- errors.New("length is shorter")
	}
}

func email(cErr chan<- error, s interface{})  {
	
	Re := regexp.MustCompile(`^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}$`)
	a := fmt.Sprintf("%v", s)
	if !Re.MatchString(a) {
		cErr <- errors.New("not email format")
	}
}

func customValidation(cErr chan<- error, s interface{}, param []string)  {
	Re := regexp.MustCompile(param[1])
	a := fmt.Sprintf("%v", s)
	if !Re.MatchString(a) {
		cErr <- errors.New("not in regex format")
	}
}

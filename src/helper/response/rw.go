package response

import (
	"net/http"
	"encoding/json"
)

// JSONResponseWriter create and send the JSON output
func JSONResponseWriter(w http.ResponseWriter, output map[string]interface{}, code int)  {
	w.Header().Set("content-type", "application/json")
	w.WriteHeader(code)
	
	json.NewEncoder(w).Encode(output)
}
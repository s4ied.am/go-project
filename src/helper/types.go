package helper

import (
	// "strings"
	// "errors"
	// "log"
)

type hrr struct{
	c 		chan bool
	Msg 	map[string]string `json:"message"`
	Code 	int								`json:"error code"`
}
// VErrs is a custom error object
type VErrs []*hrr

func runValidation(field string, fieldValue interface{}, rules []string, myErr *VErrs, code int)  {

	pErr := hrr{c: make(chan bool), Msg: make(map[string]string), Code: 0}
	*myErr = append(*myErr, &pErr)

	go func ()  {
		err := validationOperator(fieldValue, rules)
	
		if err != nil {
			pErr.Msg[field] = err.Error()
			pErr.Code = code
			pErr.c<-true
			return
		}

		pErr.c<-false
	}()
}


// Password is a custom type for passwords
type Password string

// Validate check the password against validation rules defined in v
func (p Password) Validate(sErr *VErrs, v ...string) {
	if len(v) == 0 {
		v = append(v, "min:8")
		// TODO: add default checklist like: length & ... for password
	}

	runValidation("password", p, v, sErr, 10)
}

// HashPass hashes the password by SHA256
func (p Password) HashPass() string {
	return "hash"
}


// Username is a custom type for Usernames
type Username string

// Validate check the username against validation rules defined in v
func (u Username) Validate(sErr *VErrs, v ...string) {
	if len(v) == 0{
		v = append(v, "min:5")
		// TODO: add default validation
	}

	runValidation("username", u, v, sErr, 20)
}


// Email is type for Email
type Email string

// Validate check validation for email
func (email Email) Validate(sErr *VErrs, v ...string){
	if len(v) == 0{
		v = append(v, "email")
		// TODO: add default validation
	}

	runValidation("email", email, v, sErr, 50)
}
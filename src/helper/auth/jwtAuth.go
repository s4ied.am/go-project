package auth

import (
	"net/http"
	"strings"
	"errors"
	"time"
	"io"
	"encoding/json"
	jwt "github.com/dgrijalva/jwt-go"
)

// WebToken custom type for holding the token strgin
type WebToken struct{
	Token string `json:"token"`
	Error error
}

// TokenStruct is a custom type for the data need to be store in jwt
type TokenStruct struct {
	UserID string `json:"userId"`
	UserName string `json:"username"`
 	jwt.StandardClaims
}

const secrectKey = "My secrect key "

// CreatToken create a token with expired time and the custom palyload defined by developer
func CreatToken(exp time.Time, c TokenStruct) (*WebToken, error) {

	claim := TokenStruct{
		c.UserID,
		c.UserName,
		jwt.StandardClaims{
			ExpiresAt: exp.Unix(),
			IssuedAt: time.Now().Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claim)

	tokenString, err := token.SignedString([]byte(secrectKey))

	if err != nil {
		return nil, err
	}

	wt := &WebToken{}
	wt.Token = tokenString
	return wt, nil

}


// Validate will return an array of payload in jwt
func (wt *WebToken) Validate() (TokenStruct, error) {

	if wt.Error != nil {
		return TokenStruct{}, wt.Error
	}

	token, err := jwt.ParseWithClaims(wt.Token, &TokenStruct{}, func(token *jwt.Token) (interface{}, error) {
		
    if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
        return nil, errors.New("Unexpected signing method: " + token.Header["alg"].(string) )
		}
		// return the secret key for validating the jwt
    return []byte(secrectKey), nil
	})

	if err != nil {
		return TokenStruct{}, err
	}

	
	
	if claims, ok := token.Claims.(*TokenStruct); ok && token.Valid {
		return *claims, nil
	}
	
	return TokenStruct{}, err

}


// GetTokenFromRequest get the token from request body
func GetTokenFromRequest(r *http.Request, body io.Reader) (*WebToken) {
	token := &WebToken{}
	err := json.NewDecoder(body).Decode(token)
	
	if len(token.Token) < 1 {
		tokenString := strings.TrimPrefix(r.Header.Get("Authorization"), "Bearer ")

		if len(tokenString) > 0 {
			return &WebToken{Token: tokenString, Error: nil}
		}
	}
	
	if err != nil || len(token.Token) < 1 {		
		token.Error = errors.New("token is not set in request")
	}
	return token
}
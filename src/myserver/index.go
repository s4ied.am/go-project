package myserver

import(
	"fmt"
	"net/http"
	"web"
)

// RunServer starts the server
func RunServer()  {
	http.HandleFunc("/", web.Router)
	
	fmt.Println("server is up")

	http.ListenAndServe(":8080", nil)
}
